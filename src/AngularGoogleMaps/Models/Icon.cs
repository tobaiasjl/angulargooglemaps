﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AngularGoogleMaps.Models
{
    public class Icon
    {
        public Uri Image { get; set; }

        public Uri ShadowImage { get; set; }

        public Size Size { get; set; }

        public Anchor Anchor { get; set; }
    }
}
