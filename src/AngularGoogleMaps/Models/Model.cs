﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Diagnostics;

namespace AngularGoogleMaps
{
    [DebuggerDisplay("{Latitude},{Longitude},{Zoom}")]
    public class Model
    {
        public decimal Latitude { get; set; }

        public decimal Longitude { get; set; }

        public int Zoom { get; set; }

        public Models.Icon Icon { get; set; }

        public Models.Format Format { get; set; }

        public Model()
        {

        }

        public Model(Model other)
        {
            Latitude = other.Latitude;
            Longitude = other.Longitude;
            Zoom = other.Zoom;
            Icon = other.Icon;
        }

        private static bool Csv2Model(Model location, string value)
        {
            if (value == null)
            {
                return false;
            }

            var args = ((string)value).Split(',');
            if (args.Length < 3)
            {
                return false;
            }

            decimal real;
            int discrete;

            location.Latitude = decimal.TryParse(args[0], NumberStyles.Any, CultureInfo.InvariantCulture, out real) ? real : 0M;
            location.Longitude = decimal.TryParse(args[1], NumberStyles.Any, CultureInfo.InvariantCulture, out real) ? real : 0M;
            location.Zoom = int.TryParse(args[2], out discrete) ? discrete : 1;

            if (args.Length > 3)
            {
                location.Icon = new Models.Icon()
                {
                    Size = new Models.Size(),
                    Anchor = new Models.Anchor()
                };

                location.Icon.Image = (args.Length > 3) ? new Uri(args[3]) : null;
                location.Icon.ShadowImage = (args.Length > 4 && !string.IsNullOrWhiteSpace(args[4])) ? new Uri(args[4]) : null;
                location.Icon.Size.Width = (args.Length > 6 && int.TryParse(args[5], out discrete)) ? discrete : 0;
                location.Icon.Size.Height = (args.Length > 6 && int.TryParse(args[6], out discrete)) ? discrete : 0;
                if (args.Length > 8)
                {
                    location.Icon.Anchor.Horizontal = args[7];
                    location.Icon.Anchor.Vertical = args[8];

                    if (args.Length > 9)
                    {
                        if (int.TryParse(args[9], out discrete))
                        {
                            location.Format = (Models.Format) discrete;
                        }
                        else
                        {
                            Models.Format format;
                            if (Enum.TryParse<Models.Format>(args[9], true, out format))
                                location.Format = format;
                        }
                    }
                }
            }
            return true;
        }

        private static bool Model2Csv(Model location, ref string value)
        {
            if (location.Zoom < 1)
            {
                value = "";
                return false;
            }

            StringBuilder output = new StringBuilder();
            output.Append(Math.Round(location.Latitude, 6).ToString(CultureInfo.InvariantCulture));
            output.Append(',');
            output.Append(Math.Round(location.Longitude, 6).ToString(CultureInfo.InvariantCulture));
            output.Append(',');
            output.Append(location.Zoom);
            if (location.Icon != null && location.Icon.Image != null && !string.IsNullOrWhiteSpace(location.Icon.Image.AbsoluteUri))
            {
                output.Append(',');
                output.Append(location.Icon.Image.AbsoluteUri);
                output.Append(',');
                if (location.Icon.ShadowImage != null && !string.IsNullOrWhiteSpace(location.Icon.ShadowImage.AbsoluteUri))
                    output.Append(location.Icon.ShadowImage.AbsoluteUri);
                output.Append(',');
                output.Append(location.Icon.Size.Width);
                output.Append(',');
                output.Append(location.Icon.Size.Height);
                output.Append(',');
                output.Append((string) location.Icon.Anchor.Horizontal);
                output.Append(',');
                output.Append((string) location.Icon.Anchor.Vertical);
                output.Append(',');
                output.Append(location.Format.ToString().ToLowerInvariant());
            }
            value = output.ToString();
            return true;
        }

        public static implicit operator Model(string value)
        {
            return new Model(value);
        }

        public Model(string value, bool isCsv = true)
        {
            if (string.IsNullOrWhiteSpace(value))
                return;

            if (value[0] == '{')
                Json2Model(this, value);
            else
                Csv2Model(this, value);
        }

        public static implicit operator string(Model location)
        {
            string value = "";
            Model2Csv(location, ref value);
            return value;
        
        }

        public override string ToString()
        {
            string value = "";
            Model2Csv(this, ref value);
            return value;
        }

        private static bool Model2Json(Model location, ref string value)
        {
            StringBuilder output = new StringBuilder();
            output.Append("{'latitude':");
            output.Append(Math.Round(location.Latitude, 6).ToString(CultureInfo.InvariantCulture));
            output.Append(",'longitude':");
            output.Append(Math.Round(location.Longitude, 6).ToString(CultureInfo.InvariantCulture));
            output.Append(",'zoom':");
            output.Append(location.Zoom);
            if (location.Icon.Image != null && !string.IsNullOrWhiteSpace(location.Icon.Image.AbsolutePath))
            {
                output.Append(",'icon':{'image':'");
                output.Append(location.Icon.Image);
                if (location.Icon.ShadowImage != null && !string.IsNullOrWhiteSpace(location.Icon.ShadowImage.AbsolutePath))
                {
                    output.Append("','shadow':'");
                    output.Append(location.Icon.ShadowImage);
                }
                output.Append("','size':{'width':");
                output.Append(location.Icon.Size.Width);
                output.Append(",'height':");
                output.Append(location.Icon.Size.Height);
                output.Append("},'anchor':{'horizontal':");
                if (location.Icon.Anchor.Horizontal.IsManual())
                    output.Append(location.Icon.Anchor.Horizontal.Manual);
                else
                {
                    output.Append('\'');
                    output.Append(location.Icon.Anchor.Horizontal.Automatic);
                    output.Append('\'');
                }
                output.Append(",'vertical':");
                if (location.Icon.Anchor.Vertical.IsManual())
                    output.Append(location.Icon.Anchor.Vertical.Manual);
                else
                {
                    output.Append('\'');
                    output.Append(location.Icon.Anchor.Vertical.Automatic);
                    output.Append('\'');
                }
                output.Append("}}");
            }
            output.Append(",'format':'");
            output.Append(location.Format.ToString().ToLowerInvariant());
            output.Append("\'}");
            value = output.ToString();
            return  true;
        }

        private static string Json2ModelStringValue(string json, string value)
        {
            int start = 0;
            int pos;
            while ((pos = json.IndexOf(value, start, StringComparison.InvariantCultureIgnoreCase)) != -1)
            {
                pos += value.Length;

                if (json[pos] == '\'' || json[pos] == '\"')
                    pos++;
                if (json[pos] != ':')
                {
                    start = pos;
                    continue;
                }
                pos++;
                char delimiter = '\0';
                if (json[pos] == '\'' || json[pos] == '\"')
                    delimiter = json[pos++];

                var text = new StringBuilder();

                while (pos < json.Length)
                {
                    if (json[pos] == delimiter || (delimiter == '\0' && (json[pos] == ',' || json[pos] == '}')))
                        return text.ToString();
                    text.Append(json[pos++]);
                }
            }
            return null;
        }

        private static int? Json2ModelIntValue(string json, string value)
        {
            var text = Json2ModelStringValue(json, value);

            if (string.IsNullOrWhiteSpace(text))
                return null;

            int discrete;
            if (!int.TryParse(text, out discrete))
                return null;
            return discrete;
        }

        private static decimal? Json2ModelDecimalValue(string json, string value)
        {
            var text = Json2ModelStringValue(json, value);

            if (string.IsNullOrWhiteSpace(text))
                return null;

            decimal real;
            if (!decimal.TryParse(text, NumberStyles.Any, CultureInfo.InvariantCulture, out real))
                return null;
            return real;
        }

        private static bool Json2Model(Model location, string value)
        {
            decimal? lat, lng;
            int? zoom;
            if ((lat = Json2ModelDecimalValue(value, "latitude")) == null ||
                (lng = Json2ModelDecimalValue(value, "longitude")) == null ||
                (zoom = Json2ModelIntValue(value, "zoom")) == null)
            {
                return false;
            }
            location.Latitude = (decimal) lat;
            location.Longitude = (decimal) lng;
            location.Zoom = (int) zoom;

            string image;
            if (!string.IsNullOrWhiteSpace(image = Json2ModelStringValue(value, "image")))
            {
                location.Icon = new Models.Icon()
                {
                    Size = new Models.Size(),
                    Anchor = new Models.Anchor()
                };
                location.Icon.Image = new Uri(image);
                if (!string.IsNullOrWhiteSpace(image = Json2ModelStringValue(value, "shadow")))
                {
                    location.Icon.ShadowImage = new Uri(image);
                }
                location.Icon.Size.Width = Json2ModelIntValue(value, "width") ?? 0;
                location.Icon.Size.Height = Json2ModelIntValue(value, "height") ?? 0;
                location.Icon.Anchor.Horizontal = Json2ModelStringValue(value, "horizontal");
                location.Icon.Anchor.Vertical = Json2ModelStringValue(value, "vertical");
            }
            return  true;
        }

        public string ToCsv()
        {
            string value = "";
            Model2Csv(this, ref value);
            return value;
        }

        public string ToJson()
        {
            string value = "";
            Model2Json(this, ref value);
            return value;
        }
    }
}
