﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AngularGoogleMaps.Models
{
    [DebuggerDisplay("{Horizontal} {Vertical}")]
    public class Anchor
    {
        public AnchorHorizontal Horizontal { get; set; }

        public AnchorVertical Vertical { get; set; }

        public Anchor()
        {
            Horizontal = AngularGoogleMaps.Models.AnchorHorizontal.Style.Center;
            Vertical = AngularGoogleMaps.Models.AnchorVertical.Style.Bottom;
        }
    }
}
