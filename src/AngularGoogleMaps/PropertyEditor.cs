﻿using ClientDependency.Core;
using System;
using System.Collections.Generic;
using Umbraco.Core.PropertyEditors;
using Umbraco.Web.PropertyEditors;

namespace AngularGoogleMaps
{
	public enum CoordinatesBehavour
	{
		Hide = 0,
		Show = 1,
		Edit = 2
	}

	[PropertyEditor("AngularGoogleMaps", "Angular Google Maps", "/App_Plugins/AngularGoogleMaps/1.0.8/view.html", ValueType = "TEXT")]
	[PropertyEditorAsset(ClientDependencyType.Javascript, "/App_Plugins/AngularGoogleMaps/1.0.8/controller.js")]
	[PropertyEditorAsset(ClientDependencyType.Javascript, "/App_Plugins/AngularGoogleMaps/1.0.8/angular-google-maps.js")]
	[PropertyEditorAsset(ClientDependencyType.Javascript, "/App_Plugins/AngularGoogleMaps/1.0.8/config.icon.js")]
	[PropertyEditorAsset(ClientDependencyType.Css, "/App_Plugins/AngularGoogleMaps/1.0.8/view.css")]
	public class AGMPropertyEditor : PropertyEditor
	{
		protected override PreValueEditor CreatePreValueEditor()
		{
			return new AGMPreValueEditor();
		}

		public AGMPropertyEditor()
		{
            _defaultPreVals = new Dictionary<string, object>
            {
                { "defaultLocation", "0,0,1" },
                { "height", 400 },
				{ "coordinatesBehavour", (int) CoordinatesBehavour.Edit },
				{ "icon", "{\"image\":\"https://mt.google.com/vt/icon/name=icons/spotlight/spotlight-poi.png\"}" },
                { "format", (int) Models.Format.Csv }
            };
		}

        private IDictionary<string, object> _defaultPreVals;
		public override IDictionary<string, object> DefaultPreValues
		{
			get { return _defaultPreVals; }
			set { _defaultPreVals = value; }
		}

		internal class AGMPreValueEditor : PreValueEditor
		{
			[PreValueField("defaultLocation", "Default Location", "/App_Plugins/AngularGoogleMaps/1.0.8/config.defaultlocation.html",
				Description = "Enter the default location for new map: latitude,longitude,zoom")]
			public Model DefaultLocation { get; set; }

			[PreValueField("height", "Map Height", "number",
				Description = "Enter the height of the Google map in pixels. 400 is default")]
			public int Height { get; set; }

			[PreValueField("hideSearch", "Hide Search", "boolean",
				Description = "Hide the search feature")]
			public bool HideSearch { get; set; }

			[PreValueField("hideLabel", "Hide Label", "boolean",
				Description = "Hide the Umbraco property title and description, making the map span the entire page width")]
			public bool HideLabel { get; set; }

			[PreValueField("coordinatesBehavour", "Coordinates Behavour", "/App_Plugins/AngularGoogleMaps/1.0.8/config.coordinatesbehavour.html", 
				Description = "Choose whether selected coordinates are shown and/or editable by the content editor")]
			public CoordinatesBehavour CoordinatesBehavour { get; set; }

			[PreValueField("icon", "Marker Icon", "/App_Plugins/AngularGoogleMaps/1.0.8/config.icon.html",
				Description = "Enter icon details or select predefined values from dropdown list")]

            public Models.ConfigIcon Icon { get; set; }

			[PreValueField("format", "Format", "/App_Plugins/AngularGoogleMaps/1.0.8/config.format.html",
				Description = "Enter how you wish to store the values")]

            public Models.Format Format { get; set; }

            //   Backward compatibility
            public CoordinatesBehavour coordinatesBehavour 
            { 
                get
                {
                    return CoordinatesBehavour;
                }
                set
                {
                    CoordinatesBehavour = value;
                }
            }

            public string IconUrl
            {
                get
                {
                    return Icon.Image.AbsoluteUri;
                }
                set
                {
                    Icon.Image = new Uri(value);
                }
            }



		}
	}
}
